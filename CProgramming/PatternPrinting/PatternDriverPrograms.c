//
//  PatternDriverPrograms.c
//  CProgramming
//
//  Created by RAHUL GOEL on 12/09/16.
//  Copyright © 2016 RAHUL GOEL. All rights reserved.
//

#include "PatternDriverPrograms.h"
#include "Header_FilesIncluded.h"

void printAllPatterns(int n){
    pattern1(n);
    pattern2(n);
    pattern3(n);
    pattern4(n);
    pattern5(n);
    pattern6(n);
    pattern7(n);
    pattern8(n);
    pattern9(n);
    pattern10(n);
    pattern11(n);
    pattern12(10);
}