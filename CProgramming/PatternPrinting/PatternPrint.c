//
//  1.c
//  CProgramming
//
//  Created by RAHUL GOEL on 25/05/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

#include "PatternPrint.h"

//3.Print the Star Pyramid (n:variable)
void printStarPyramid(int n)
{
    for (int i=0; i<n; i++)
    {
        for (int j=0; j<=n-i; j++)
        {
            if (j==n-i-1)
                
            {
                for (int k=0; k<2*i+1; k++)
                    
                {
                    printf("*");
                }
            }
            else
            {
                printf(" ");
            }
        }
        printf("\n");
    }
}


void printStarPyramidWithAlternateAInIt(int n)
{
    for (int i=0; i<n; i++)
    {
        for (int j=0; j<=n-i; j++)
        {
            if (j==n-i-1)
                
            {
                for (int k=0; k<2*i+1; k++)
                    
                {
                    if (k%2==0)
                        
                    {
                        printf("*");
                    }
                    else
                    {
                        printf("A");
                    }
                }
            }
            else
            {
                printf(" ");
            }
        }
        printf("\n");
    }
}


void pattern1(int n){
    printf("\n\n---- One ----\n\n");
    for (int i=1; i<=n; i++) {
        for (int j=1; j<=n; j++) {
            printf("*");
        }
        printf("\n");
    }
}

void pattern2(int n){
    printf("\n\n---- Two ----\n\n");
    for (int i=1; i<=n; i++) {
        for (int j =1 ; j<= i; j++) {
            printf("*");
        }
        printf("\n");
    }
}

void pattern3(int n){
    printf("\n\n---- Three ----\n\n");
    for (int i=1; i<=n; i++) {
        for (int j=1; j<=n; j++) {
            if (j>n-i){
                printf("*");
            }
            else{
                printf(" ");
            }
        }
        printf("\n");
    }
}

void pattern4(int n){
    printf("\n\n---- Four ----\n\n");
    for (int i=1; i<=n; i++) {
        for (int j=1; j<=n; j++) {
            if (j>=i){
                printf("*");
            }else{
                printf(" ");
            }
        }
        printf("\n");
    }
}

void pattern5(int n){
    printf("\n\n---- Five ----\n\n");
    for (int i =1; i<=n; i++) {
        for (int j=1; j<=n; j++) {
            if (j>n-i+1){
                printf(" ");
            }else{
                printf("*");
            }
        }
        printf("\n");
    }
    
}

void pattern6(int n){
    printf("\n\n---- Six ----\n\n");
    int a = 1;
    for (int i =1; i<=n; i++) {
        for (int j=1; j<=i; j++) {
            printf("%d ",a);
            a++;
        }
        printf("\n");
    }

}

void pattern7(int n){
    printf("\n\n---- Seven ----\n\n");
    for (int i=1; i<=n; i++) {
        for (int j=1; j<=n; j++) {
            if (j<=n-i){
                printf(" ");
            }else{
                printf("*");
            }
        }
        printf("\n");
    }
    for (int i=1; i<n; i++) {
        for (int j=1; j<=n; j++) {
            if (j<=i){
                printf(" ");
            }else{
                printf("*");
            }
        }
        printf("\n");
    }
}


void pattern8(int n){
    printf("\n\n---- Eight ----\n\n");
    for (int i =1; i<=n; i++) {
        for (int j=1; j<=n-i+1; j++) {
            printf("%d ",i);
        }
        printf("\n");
    }
}


void pattern9(int n){
    printf("\n\n---- Nine ----\n\n");
    for (int i =1; i<=n; i++) {
        for (int j=1; j<=2*n-1; j++) {
            if (j<i || j>(2*n-i)){
                printf(" ");
            }else{
            printf("*");
            }
        }
        printf("\n");
    }
}


void pattern10(int n){
    printf("\n\n---- Ten ----\n\n");
    for (int i =1; i<=n; i++) {
        for (int j=1; j<=2*n-1; j++) {
            if (j<i || j>(2*n-i)){
                printf("*");
            }else{
                printf(" ");
            }
        }
        printf("\n");
    }
}

void pattern11(int n){
    printf("\n\n---- Eleven ----\n\n");
    for (int i =1; i<=n; i++) {
        for (int j=1; j<=2*n; j++) {
            if (j>n-i && j<n+i){
                printf("*");
            }else{
                printf(" ");
            }
        }
        printf("\n");
    }
}


void pattern12(int n){
    int arrary[10] = {2,3,4,6,9,2,4,2,7,1};
    int max = 9;
    int numberOfElements = 10;
    printf("\n\n---- Tweleve ----\n\n");
    for (int i =0; i<max; i++) {
        for (int j=0; j<numberOfElements; j++) {
            if (arrary[j]>0){
                printf("*");
            }else{
                printf(" ");
            }
            arrary[j] = arrary[j] - 1;
        }
        printf("\n");
    }
}


