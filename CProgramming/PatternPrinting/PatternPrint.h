//
//  PatternPrint.h
//  CProgramming
//
//  Created by RAHUL GOEL on 25/05/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//


#include <stdio.h>

/*
 2.Print the Floyd's Triangle (n:variable)
 1
 23
 456
 789
 10111213
 */


void printStarPyramid(int n);
/*
3.Print the Star Pyramid (n:variable)
    *
   ***
  *****
 *******
*********
*/


/*
 4.Print the Pascal Triangle (n:variable)
        1
       1  1
     1  2  1
   1  3  3  1
 
*/

void printStarPyramidWithAlternateAInIt(int n);
/*
5.Print the Star Pyramid With Alternate 'A' in it(n:variable)
     *
    *A*
   *A*A*
  *A*A*A*
 *A*A*A*A*

*/


void pattern1(int n);

void pattern2(int n);

void pattern3(int n);

void pattern4(int n);

void pattern5(int n);

void pattern6(int n);

void pattern7(int n);

void pattern8(int n);

void pattern9(int n);

void pattern10(int n);

void pattern11(int n);

void pattern12(int n);



