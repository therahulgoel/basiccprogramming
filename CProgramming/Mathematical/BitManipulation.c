//
//  BitManipulation.c
//  CProgramming
//
//  Created by RAHUL GOEL on 17/04/16.
//  Copyright (c) 2016 RAHUL GOEL. All rights reserved.
//

#include "BitManipulation.h"

//To Show the Use of Bitwise Left Shitft and Right Shift Operators
void bitManipulation_1(){
    printf("\n ---- Program 1 ----\n");
    int a = 2,b=3;
    printf("%d %d\n",a<<1,b<<1);  //Bitwise Left Shift  or Multiply by 2*i
    printf("%d %d\n",a>>1,b>>1);  //Bitwise Right Shift  or Divice by 2*i 
}

//To convert Decimal to Binary
void bitManipulation_2(int n){
    printf("\n ---- Program 2 ----\n");
    printf("\nFor %d Decimal To Binary is LSB To MSB: \n",n);
    int remainder = 0;
    while (n>=1) {
        remainder = n%2;
        printf("%d",remainder);
        n = n/2;
    }
}

//To check if the Given Number is a Power of 2 or not
void bitManipulation_3(int n){
    printf("\n ---- Program 2 ----\n");
    printf("\nCheck If Given Number is Power Of Two Or Not : %d\n",n);
    int result = (n &&(!(n & (n-1))));
    if (result != 0) {
        printf("Power Of Two\n");
    }
    else{
        printf("Not A Power Of Two\n");
    }
}

