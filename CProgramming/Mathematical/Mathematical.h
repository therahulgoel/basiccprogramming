//
//  Mathematical.h
//  CProgramming
//
//  Created by RAHUL GOEL on 17/04/16.
//  Copyright (c) 2016 RAHUL GOEL. All rights reserved.
//

#ifndef __CProgramming__Mathematical__
#define __CProgramming__Mathematical__

#include <stdio.h>

#endif /* defined(__CProgramming__Mathematical__) */

//1. Goldbach's Conjecture : Every even integer greater than 2 can be expressed as the sum of two primes. Write a function which takes a number as input, verify if is an even number greater than 2 and also print atleast one pair of prime numbers.

