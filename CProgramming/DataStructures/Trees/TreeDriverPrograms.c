//
//  TreeDriverPrograms.c
//  CProgramming
//
//  Created by RAHUL GOEL on 06/09/16.
//  Copyright © 2016 RAHUL GOEL. All rights reserved.
//

#include "TreeDriverPrograms.h"


void driver_tree_PreOrder(){
    printf("\nPre-Order Is : ");
    tree_PreOrder(tree_createSampleTreeWith5Nodes());
}

void driver_tree_PostOrder(){
    printf("\nPost-Order Is : ");
    tree_PostOrder(tree_createSampleTreeWith5Nodes());
}

void driver_tree_InOrder(){
    printf("\nIn-Order Is : ");
    tree_InOrder(tree_createSampleTreeWith5Nodes());
}