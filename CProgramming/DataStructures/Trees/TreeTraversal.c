//
//  TreeTraversal.c
//  CProgramming
//
//  Created by RAHUL GOEL on 17/08/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

#include "TreeTraversal.h"

/* A binary tree node has data, pointer to left child
 and a pointer to right child */
struct node{
    int data;
    struct node *left;
    struct node *right;
};


/* Helper function that allocates a new node with the
 given data and NULL left and right pointers. */
struct node* newNode(int data){
    struct node* node = (struct node*)malloc(sizeof(struct node));
    node->data = data;
    node->left = NULL;
    node->right = NULL;
    return(node);
}


/* To create A sample Tree With 5 Nodes as Below !
          1
        /   \
       2     3
      /  \
     4    5
*/
struct node* tree_createSampleTreeWith5Nodes(){
    struct node *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    return root;
}

void tree_PreOrder(struct node *root){
    if (root == NULL) {
        return;
    }
    else{
        printf("%d ",root->data);
        tree_PreOrder(root->left);
        tree_PreOrder(root->right);
    }
}

void tree_PostOrder(struct node *root){
    if (root == NULL) {
        return;
    }
    else{
        tree_PostOrder(root->left);
        tree_PostOrder(root->right);
        printf("%d ",root->data);
    }
}

void tree_InOrder(struct node *root){
    if (root == NULL) {
        return;
    }
    else{
        tree_InOrder(root->left);
        printf("%d ",root->data);
        tree_InOrder(root->right);
    }
}



