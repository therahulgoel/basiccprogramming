//
//  TreeTraversal.h
//  CProgramming
//
//  Created by RAHUL GOEL on 17/08/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

#ifndef __CProgramming__TreeTraversal__
#define __CProgramming__TreeTraversal__

#include <stdio.h>
#include <stdlib.h>

#endif /* defined(__CProgramming__TreeTraversal__) */

/*1. To create A sample Tree With 5 Nodes as Below !
     1
   /   \
  2     3
 / \
4   5
*/
struct node* tree_createSampleTreeWith5Nodes();

//2.To print the Pre-order of Tree ,---Time :  Space :---
void tree_PreOrder(struct node *root);

//3.To print the Post-Order of Tree ,---Time :  Space :---
void tree_PostOrder();

//4.To print the In-Order of Tree ,---Time :  Space :---
void tree_InOrder();

//5.To Calculate Number of Leaf Nodes of given tree ,---Time :  Space :---
void tree_calculateNumberOfLeafNodes();

//6.To Calculate Number of Internal Nodes of given tree ,---Time :  Space :---
void tree_calculateNumberOfInternaNodes();

//7.To find the maximum depth or height of a tree ,---Time :  Space :---
void tree_maximumDepthOrHeight();

//8.To check if it's a Strict Binary Tree Or Not ,---Time :  Space :---
void tree_checkIfItsStrictBinaryTreeOrNot();

//9. To Print the Tree given With The Root Address :
void tree_printTreeNodes();









