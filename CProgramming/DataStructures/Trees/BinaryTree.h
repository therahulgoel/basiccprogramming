//
//  BinaryTree.h
//  CProgramming
//
//  Created by RAHUL GOEL on 20/08/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

#ifndef __CProgramming__BinaryTree__
#define __CProgramming__BinaryTree__

#include <stdio.h>
#include <stdlib.h>

#endif /* defined(__CProgramming__BinaryTree__) */

//To Create A binary Tree With Some Nodes Added Already ! ,---Time :  Space :---
void binaryTree_withSomeNodesAdded();

//To do the Boundry Traversal for the given Binary Tree
void binaryTree_BoundryTraversal();

