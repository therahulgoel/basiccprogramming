//
//  BinaryTree.c
//  CProgramming
//
//  Created by RAHUL GOEL on 20/08/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

#include "BinaryTree.h"

struct BTNode
{
    int value;
    struct BTNode *leftCPtr;
    struct BTNode *rightCPtr;
};
struct BTNode *rootBT;

void binaryTree_withSomeNodesAdded()
{
    rootBT=(struct BTNode *)malloc(sizeof(struct BTNode));
    
    struct BTNode *node1=(struct BTNode *)malloc(sizeof(struct BTNode));
    struct BTNode *node2=(struct BTNode *)malloc(sizeof(struct BTNode));
    struct BTNode *node3=(struct BTNode *)malloc(sizeof(struct BTNode));
    struct BTNode *node4=(struct BTNode *)malloc(sizeof(struct BTNode));
    
    rootBT->value=1;
    node1->value=2;
    node2->value=3;
    node3->value=4;
    node4->value=5;

    rootBT->leftCPtr=node1;
    rootBT->rightCPtr=node2;
    node1->leftCPtr=node3;
    node1->rightCPtr=node4;
}


void binaryTree_BoundryTraversal(){

}


