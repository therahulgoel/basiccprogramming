//
//  TreeDriverPrograms.h
//  CProgramming
//
//  Created by RAHUL GOEL on 06/09/16.
//  Copyright © 2016 RAHUL GOEL. All rights reserved.
//

#ifndef TreeDriverPrograms_h
#define TreeDriverPrograms_h

#include <stdio.h>
#include "Header_FilesIncluded.h"

#endif /* TreeDriverPrograms_h */

//**** Tree Traversal ****
//1.
void driver_tree_PreOrder();
//2.
void driver_tree_PostOrder();
//1.
void driver_tree_InOrder();

