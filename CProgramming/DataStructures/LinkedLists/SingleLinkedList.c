//
//  SingleLinkedList.c
//  CProgramming
//
//  Created by RAHUL GOEL on 17/08/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

#include "SingleLinkedList.h"
#include <stdlib.h>

//Structure To Be used to create Single Linked List
struct data
{
    int value;
    struct data *nextAddress;
};
struct data *startAddress;

//To Store the Number Of Nodes in the Current Single Linked List
int totalNumberOfNodes;

void singleLinkedList_print()
{
    if (startAddress!=NULL)
    {
        if (startAddress->nextAddress==NULL)  //For "Single" Node
        {
            printf(" %d-> ",startAddress->value);
        }
        else //For More Then Single Node
        {
            while (startAddress->nextAddress!=NULL)
            {
                printf(" %d-> ",startAddress->value);
                startAddress=startAddress->nextAddress;
            }
            printf(" %d-> ",startAddress->value);
        }
        startAddress=startAddress->nextAddress;
    }
    else  //Means There is no Node At All !
    {
        printf("***Single Linked List Is Empty !***\n");
    }
}

void singleLinkedList_insertNodeInTheEnd(int value)
{
    if (startAddress==NULL)  //No Node
    {
        startAddress=(struct data *)malloc(sizeof(struct data));
        startAddress->value=value;
        startAddress->nextAddress=NULL;
        totalNumberOfNodes=totalNumberOfNodes+1;
    }
    else
    {
        struct data *nodeToBeInserted,*somePtr;
        somePtr=startAddress;  //Base Address To Be Stored !
        while (somePtr->nextAddress!=NULL)
        {
            somePtr=somePtr->nextAddress;
        }
        nodeToBeInserted=(struct data *)malloc(sizeof(struct data));
        somePtr->nextAddress=nodeToBeInserted;
        nodeToBeInserted->value=value;
        nodeToBeInserted->nextAddress=NULL;
        totalNumberOfNodes=totalNumberOfNodes+1;
    }
}

void singleLinkedList_insertNodeAtAPosition(int value,int position)
{
    struct data *onePtrBefore,*onePtrAfter,*nodeToBeInserted;
    int numberOfNodes=0;
    onePtrBefore=startAddress;    //Base Address To Be Stored !
    while (onePtrBefore->nextAddress!=NULL && numberOfNodes<=position)
    {
        onePtrBefore=onePtrBefore->nextAddress;
        onePtrAfter=onePtrBefore->nextAddress;
        numberOfNodes=numberOfNodes+1;
        if (numberOfNodes==position)
        {
            nodeToBeInserted=(struct data *)malloc(sizeof(struct data));
            nodeToBeInserted->value=value;
            onePtrBefore->nextAddress=nodeToBeInserted;
            nodeToBeInserted->nextAddress=onePtrAfter;
        }
        totalNumberOfNodes=totalNumberOfNodes+1;
    }
}

void singleLinkedList_totalNumberOfNodes()
{
    printf("\nTotal Number Of Nodes : %d\n",totalNumberOfNodes);
}

void singleLinkedList_deleteNodeFromSpecifiedPosition(int position)
{
    int count=0;
    struct data *onePtrBefore,*onePtrAfter=NULL,*nodeToBDeleted=NULL;
    onePtrBefore=startAddress;
    if (position<totalNumberOfNodes)
    {
        while (count<=position)
        {
            nodeToBDeleted=onePtrBefore->nextAddress;
            onePtrAfter=nodeToBDeleted->nextAddress;
            count=count+1;
        }
        onePtrBefore->nextAddress=onePtrAfter;
        free(nodeToBDeleted);
    }
    else
    {
        printf("\nOops ! No such Position Available !");
    }
}


void singleLinkedList_WithFiveNodesAlreadyAdded()
{
    startAddress=(struct data *)malloc(sizeof(struct data));
    
    struct data *second=NULL;
    second=(struct data *)malloc(sizeof(struct data));

    struct data *third=NULL;
    third=(struct data *)malloc(sizeof(struct data));

    struct data *fourth=NULL;
    fourth=(struct data *)malloc(sizeof(struct data));

    struct data *fifth=NULL;
    fifth=(struct data *)malloc(sizeof(struct data));
    
    startAddress->nextAddress=second;
    second->nextAddress=third;
    third->nextAddress=fourth;
    fourth->nextAddress=fifth;
    
    startAddress->value=10;
    second->value=20;
    third->value=30;
    fourth->value=40;
    fifth->value=50;
}
