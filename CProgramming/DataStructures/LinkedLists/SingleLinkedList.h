//
//  SingleLinkedList.h
//  CProgramming
//
//  Created by RAHUL GOEL on 17/08/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

#ifndef __CProgramming__SingleLinkedList__
#define __CProgramming__SingleLinkedList__

#include <stdio.h>

#endif /* defined(__CProgramming__SingleLinkedList__) */


//To Print out all the Nodes created in Single Linked list ! ,---Time :  Space :---
void singleLinkedList_print();

//To insert Node In the End/append of Linked List ! ,---Time :  Space :---
void singleLinkedList_insertNodeInTheEnd(int value);

//To insert Node At a specified position ! ,---Time :  Space :---
void singleLinkedList_insertNodeAtAPosition(int value,int position);

//To print the total number of nodes ,---Time :  Space :---
void singleLinkedList_totalNumberOfNodes();

//To Delete A Node From Specified position ! ,---Time :  Space :---
void singleLinkedList_deleteNodeFromSpecifiedPosition(int position);

//To create list with Five Nodes Already Added ! ,---Time :  Space :---
void singleLinkedList_WithFiveNodesAlreadyAdded();


