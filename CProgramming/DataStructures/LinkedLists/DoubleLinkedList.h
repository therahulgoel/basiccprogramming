//
//  DoubleLinkedList.h
//  CProgramming
//
//  Created by RAHUL GOEL on 17/08/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

#ifndef __CProgramming__DoubleLinkedList__
#define __CProgramming__DoubleLinkedList__

#include <stdio.h>

#endif /* defined(__CProgramming__DoubleLinkedList__) */

//To Print out all the Nodes created in Double Linked list ! ,---Time :  Space :---
void doubleLinkedList_print();

//To insert Node In the End/append of Linked List ! ,---Time :  Space :---
void doubleLinkedList_insertNodeInTheEnd(int value);

//To insert Node At a specified position ! ,---Time :  Space :---
void doubleLinkedList_insertNodeAtAPosition(int value,int position);

//To print the total number of nodes ,---Time :  Space :---
void doubleLinkedList_totalNumberOfNodes();

//To Delete A Node From Specified position ! ,---Time :  Space :---
void doubleLinkedList_deleteNodeFromSpecifiedPosition(int position);
