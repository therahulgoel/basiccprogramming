//
//  SomeCoolArrayProblems.c
//  CProgramming
//
//  Created by RAHUL GOEL on 17/08/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

#include "SomeCoolArrayProblems.h"

void findMissingNumber()
{
    int sum=0,n=0,sumOfAllArrayElements=0,missingElement=0;
    int arrayOfNumbers[10]={1,6,9,5,4,3,2,7,10};   //Let Missing Number Is 8
    n=10;  //Number of elements
    
    sum=n*(n+1)/2;  //To find out sum of all numbers from 1 to 10
    
    for (int i=0; i<n; i++) {  //Sum of given array elements
        sumOfAllArrayElements=sumOfAllArrayElements+arrayOfNumbers[i];
    }
    
    missingElement=sum-sumOfAllArrayElements;
    printf("\nMissing Element Is = %d",missingElement);
}


void findSumOfAllNumbersInArray(){
    //To get the Array with n elements as an input from user line by line !
    int n,sum =0;
    scanf("%d",&n);
    int arr[n];
    for (int i=0; i<n; i++) {
        scanf("%d",&arr[i]);
        sum = sum + arr[i];
    }
    printf("Sum is %d",sum);
}

void printAllTheElementsOfInputArray(int arr[], int n){
    //Printing out that array
    for (int i=0; i<n; i++) {
        printf("%d ",arr[i]);
    }
}

