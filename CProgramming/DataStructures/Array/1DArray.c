//
//  1DArray.c
//  CProgramming
//
//  Created by RAHUL GOEL on 26/09/16.
//  Copyright © 2016 RAHUL GOEL. All rights reserved.
//

#include "1DArray.h"

//To Sort an array of 0's and 1's in O(N) time
//To Segregate 0's and 1's in the given array in O(N) time 
void array1d_1(){
    int arr[8] = {0,0,1,0,0,1,1,0};
    int countOf0 = 0, countOf1 = 0;
    for (int i =0; i<8; i++) {
        if (arr[i] == 1){
            countOf1 = countOf1 + 1;
        }else{
            countOf0 = countOf0 + 1;
        }
    }
    
    for (int i =0; i<countOf0;i++ ) {
        arr[i] = 0;
    }
    for (int i =0; i<countOf1; i++) {
        arr[countOf0+i] = 1;
    }
    for (int i = 0; i<8; i++) {
        printf("%d",arr[i]);
    }
}

