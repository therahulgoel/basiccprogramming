//
//  2DMatrixDriverProgram.c
//  CProgramming
//
//  Created by RAHUL GOEL on 21/09/16.
//  Copyright © 2016 RAHUL GOEL. All rights reserved.
//

#include "2DMatrixDriverProgram.h"

void printAll2DMatricsPrograms(){
    array2d_1();
    array2d_2();
    array2d_3();
    array2d_4();
    array2d_5();
    array2d_6();
}