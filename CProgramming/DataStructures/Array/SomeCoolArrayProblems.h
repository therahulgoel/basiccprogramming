//
//  SomeCoolArrayProblems.h
//  CProgramming
//
//  Created by RAHUL GOEL on 17/08/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

#ifndef __CProgramming__SomeCoolArrayProblems__
#define __CProgramming__SomeCoolArrayProblems__

#include <stdio.h>

#endif /* defined(__CProgramming__SomeCoolArrayProblems__) */


/*1. To print given 2-D Matrix in Spiral Form !
Input:
1    2   3   4
5    6   7   8
9   10  11  12
13  14  15  16
Output:
1 2 3 4 8 12 16 15 14 13 9 5 6 7 11 10
 */


//2. Finding Missing Number  ---Time : O(n)  Space :---O(n) You are given a list of n-1 integers and these integers are in the range of 1 to n. There are no duplicates in list. One of the integers is missing in the list. Write the program to find that missing number

 //Example:
 //Input  :    [1, 2, 4, ,6, 3, 7, 8]
 //Output :    5
void findMissingNumber();


//3. Given an  array of integers, and a number ‘sum’, find the number of pairs of integers in the array whose sum is equal to sum.
void countNumberOfPairsWithGivenSumInArray();


//4. To print out all the elements of the given input array
void printAllTheElementsOfInputArray(int arr[],int n);


//5. To print out the maximum element from the array
void printMaximumElementInAnArray();

 

