//
//  2DMatrix.c
//  CProgramming
//
//  Created by RAHUL GOEL on 21/09/16.
//  Copyright © 2016 RAHUL GOEL. All rights reserved.
//

#include "2DMatrix.h"

int mat1[3][3] = {{1,2,3},{4,5,6},{7,8,9}};
int mat2[3][3] = {{2,2,2},{2,2,2},{2,2,2}};

//To Print The Given Two Matrics
void array2d_1(){
    printf("\n\n**** 2D Matrix Program 1****");
    printf("\nMatrix 1\n\n");
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            printf("%d ",mat1[i][j]);
        }
        printf("\n");
    }
    printf("\nMatrix 2\n\n");
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            printf("%d ",mat2[i][j]);
        }
        printf("\n");
    }
}

//To Print Sum of the Given Matrics
void array2d_2(){
    printf("\n**** 2D Matrix Program 2****");
    printf("\nThe Sum Is :\n");
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            printf("%d ",mat1[i][j]+mat2[i][j]);
        }
        printf("\n");
    }
}

//To Print Transpose of the Given Matrics
void array2d_3(){
    printf("\n**** 2D Matrix Program 3****");
    printf("\nThe Transpose For Matrix 1 :\n");
}

//To print matrix in Spiral Form
void array2d_4(){
    printf("\n**** 2D Matrix Program 4****");
    printf("\nSpiral Matrix 1 :\n");
    int j= 0;
    for (int i=0; i<3*3; i++) {
        if (i == 0 && j<3){
            printf("%d ",mat1[i][j]);
            j++;
        }
    }
}

//To Serach a number in row-column wise sorted matrix
void array2d_5(){
    int numbertoBeSerarched = 9;
    int foundOrNot = 0;
    printf("\n**** 2D Matrix Program 5****");
    printf("\nTo Serach a number in row-column wise sorted matrix\n");
    for (int i=0 ; i<3; i++) {
        for (int j=0; j<3; j++) {
            if (mat1[i][j]<numbertoBeSerarched ) {
                i++;
                j=0;
            }
            if (numbertoBeSerarched == mat1[i][j]){
                foundOrNot = 1;
                i = 3;
                break;
            }
        }
    }
    
    if (foundOrNot == 1){
        printf("Found It");
    }else{
        printf("Not Found");
    }

}

void array2d_6(){

}

