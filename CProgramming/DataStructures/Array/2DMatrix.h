//
//  2DMatrix.h
//  CProgramming
//
//  Created by RAHUL GOEL on 21/09/16.
//  Copyright © 2016 RAHUL GOEL. All rights reserved.
//

#ifndef _DMatrix_h
#define _DMatrix_h

#include <stdio.h>

#endif /* _DMatrix_h */

//To Print The Given Two Matrics
void array2d_1();

//To Print Sum of the Given Matrics
void array2d_2();

//To Print Transpose of the Given Matrics
void array2d_3();

//To print matrix in Spiral Form
void array2d_4();

//To Serach a number in row-column wise sorted matrix
void array2d_5();

void array2d_6();


