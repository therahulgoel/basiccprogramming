//
//  Stack.h
//  CProgramming
//
//  Created by RAHUL GOEL on 17/08/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

#ifndef __CProgramming__Stack__
#define __CProgramming__Stack__

#include <stdio.h>

#endif /* defined(__CProgramming__Stack__) */

//To POP Element Of the Stack ,---Time :  Space :---
void stack_POP();

//To PUSH Element To the Stack ,---Time :  Space :---
void stack_PUSH();

//To Print All the elements available in Stack ,---Time :  Space :---
void stack_Print();