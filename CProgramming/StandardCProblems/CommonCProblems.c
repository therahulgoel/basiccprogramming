//
//  1.c
//  CProgramming
//
//  Created by RAHUL GOEL on 25/05/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//

#include "CommonCProblems.h"

//Function to get sum of two input numbers
int sum(int a, int b)
{
    return a+b;
}

//To Calculate Factorial Of given Input
int calculateFactorial(int number)
{
    int i=0,fact=1;
    while (number>i)
    {
        fact=fact*(number-i);
        i++;
    }
    printf("Factorial of %d is = %d\n",number,fact);
    return fact;
}

//To calculate Fibonacci Series for (n:variable)
void printFibonacciSeries(int n)
{
    int first=0,second=1,sum=0,i=0;
    printf("Fibonacci Series Is : \n");
    while (i<n)
    {
        if (i<=1)
        {
            printf("%d ",i);
        }
        else
        {
            sum=first+second;
            first=second;
            second=sum;
            printf("%d ",sum);
        }
        i++;
    }
}

//Swapping of Two Numbers (Using Third Variable)
void swapNumberOneWithNumberTwoUsingThirdVariable(int numberOne,int numberTwo)
{
    printf("Numbers Before swap are %d %d\n",numberOne,numberTwo);
    int temp;
    temp=numberOne;
    numberOne=numberTwo;
    numberTwo=temp;
    printf("Numbers after swap are %d %d\n",numberOne,numberTwo);
}

void tellMeTheMachinesEndianess(){
    unsigned int i = 1;
    char *c = (char *)&i;
    if (*c) {
        printf("Little Endian");
    }
    else{
        printf("Big Endian");
    }
}







