//
//  1.h
//  CProgramming
//
//  Created by RAHUL GOEL on 25/05/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//


#include <stdio.h>

//1. Function to get sum of two input numbers (n:variable) ,---Time :
int sum(int a, int b);

//2. To Calculate Factorial Of given Input (n:variable) ,---Time :
int calculateFactorial(int n);

//3. To calculate Fibonacci Series for (n:variable) ,---Time :
void printFibonacciSeries(int n);

//4. Swapping of Two Numbers (Using Third Variable) ,---Time : 
void swapNumberOneWithNumberTwoUsingThirdVariable(int numberOne,int numberTwo);

//5. To check if the machine is Little Endian or Big Endian ?
void tellMeTheMachinesEndianess();



