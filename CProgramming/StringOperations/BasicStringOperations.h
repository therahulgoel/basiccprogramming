//
//  BasicStringOperations.h
//  CProgramming
//
//  Created by RAHUL GOEL on 25/05/15.
//  Copyright (c) 2015 RAHUL GOEL. All rights reserved.
//


#include <stdio.h>

//1. To count Number Of chars in a string ,---Time : 
int String_numberOfCharsInThisStringIs(char *str);

//2. To print reverse of a given string ,---Time :
void String_reverseOfAString(char *str);

//3. Maximum occuring character in the input string ,---Time :O(n)
char String_maximumOccuringCharIn(char *s);

//4. To print all Duplicate char in input string , --Time : O(n)
void String_printAllDuplicateCharIn(char *str);

//5. To remove all Duplicate characters from the input string --Time : O(n)
void String_removeAllDuplicateFromGivenString(char *str);

//6. To Find out first Non repeating character --Time : O(n)


//7. To Replace all spaces in input string by %20 Time : O(n)
void String_replaceAllSpacesWith_percentile20();


//8. To Remove chars from first string which are present in second string --Time:



//9. To print all permutations of the given string --Time:
void printAllPermutations(char *str);


//10. To reverse words in given input string : Let the input string be “i like this program very much”. The function should change the string to “much very program this like i”
void reverseOfWordsInSentence();


//11 . Check whether two strings are anagram of each other or not : An anagram of a string is another string that contains same characters, only the order of characters can be different. For example, “abcd” and “dabc” are anagram of each other.



//12. To print Longest Common SubSequence of two Strings


//13. Compress a string aaabbbcc into a3b3c2 (String Compression)--Time:O(n)


//14.
void string_1();

